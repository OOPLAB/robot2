/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thidarat.robotopject;

/**
 *
 * @author User
 */
public class Robot {

    private int x;
    private int y;
    private char symblo;
    private TableMap map;

    public Robot(int x, int y, char symblo, TableMap mep) {
        this.x = x;
        this.y = y;
        this.symblo = symblo;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymblo() {
        return symblo;
    }

    public boolean walk(char direction) {
        switch (direction){
            case 'N':
            case 'w':
               y=y-1;
              
                break;
            case 'S':
            case 's':
                y=y+1;
                break;
            case 'E':
            case 'd':
                x=x+1;
                break;
            case 'W':
            case 'a':
                x=x-1;
                break;
            default:
                return false;
        }
        return true;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
